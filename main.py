import pygame
import sys
import os


def get_cell(coordinates):
    """Return the cell where the player clicked."""
    x = coordinates[0]
    y = coordinates[1]
    if 0 < x <= 100 and 0 < y <= 100:
        return 1
    if 100 < x <= 200 and 0 < y <= 100:
        return 2
    if 200 < x <= 300 and 0 < y <= 100:
        return 3
    if x <= 100 and 100 < y <= 200:
        return 4
    if 100 < x <= 200 and 100 < y <= 200:
        return 5
    if 200 < x < 300 and 100 < y <= 200:
        return 6
    if 0 < x <= 100 and 200 < y <= 300:
        return 7
    if 100 < x <= 200 and 200 < y <= 300:
        return 8
    if 200 < x <= 300 and 200 < y <= 300:
        return 9
    return 0


def get_origin(cell):
    """Return the origin of the rect according to a cell"""
    if cell is 1:
        return 0, 0
    if cell is 2:
        return 100, 0
    if cell is 3:
        return 200, 0
    if cell is 4:
        return 0, 100
    if cell is 5:
        return 100, 100
    if cell is 6:
        return 200, 100
    if cell is 7:
        return 0, 200
    if cell is 8:
        return 100, 200
    if cell is 9:
        return 200, 200
    return 0, 0


def winning_combination(cells):
    """Check if the cells of the players contains a winning combination."""
    if 1 in cells and 2 in cells and 3 in cells:
        return True
    if 4 in cells and 5 in cells and 6 in cells:
        return True
    if 7 in cells and 8 in cells and 9 in cells:
        return True
    if 1 in cells and 4 in cells and 7 in cells:
        return True
    if 2 in cells and 5 in cells and 8 in cells:
        return True
    if 3 in cells and 6 in cells and 9 in cells:
        return True
    if 1 in cells and 5 in cells and 9 in cells:
        return True
    if 3 in cells and 5 in cells and 7 in cells:
        return True
    else:
        return False

width = 300
height = 300
os.environ['SDL_VIDEO_CENTERED'] = '1'
black = 0, 0, 0

pygame.init()
if pygame.display.get_init():
    pygame.display.set_caption("Tic Tac Toe")
    screen = pygame.display.set_mode((width, height), pygame.HWSURFACE)
    grid = pygame.image.load("rsc/img/grid.jpg").convert()
    square = pygame.image.load("rsc/img/square.png").convert()
    cross = pygame.image.load("rsc/img/cross.png").convert()
    screen.fill(black)
    screen.blit(grid, grid.get_rect())
    pygame.display.flip()
    while True:
        player = 1
        list_cells = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        player1_cells = list()
        player2_cells = list()
        while list_cells and not winning_combination(player1_cells) and not winning_combination(player2_cells):
            for event in pygame.event.get():
                if event.type is pygame.QUIT:
                    sys.exit()
                elif event.type is pygame.MOUSEBUTTONDOWN:
                    cell = get_cell(pygame.mouse.get_pos())
                    if cell in list_cells:
                        pygame.mixer.music.load("rsc/snd/good.wav")
                        pygame.mixer.music.play()
                        list_cells.remove(cell)
                        origin = get_origin(cell)
                        if player is 1:
                            player1_cells.append(cell)
                            screen.blit(cross, origin)
                            player = 2
                        else:
                            player2_cells.append(cell)
                            screen.blit(square, origin)
                            player = 1
                    else:
                        pygame.mixer.music.load("rsc/snd/wrong.wav")
                        pygame.mixer.music.play()
                    pygame.display.flip()
        if not list_cells:
            print("Draw")
            pygame.mixer.music.load("rsc/snd/game_over.mp3")
            pygame.mixer.music.play()
        elif winning_combination(player1_cells):
            print("Player 1 won")
            pygame.mixer.music.load("rsc/snd/win.mp3")
            pygame.mixer.music.play()
        elif winning_combination(player2_cells):
            print("Player 2 won")
            pygame.mixer.music.load("rsc/snd/win.mp3")
            pygame.mixer.music.play()
    pygame.display.quit()
